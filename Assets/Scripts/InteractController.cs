﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
[RequireComponent(typeof(Player))]
public class InteractController : MonoBehaviour
{
    [SerializeField]
    Camera targetCamera;
    
    Player player;

    Vector3 viewpoint = new Vector3(0.5f, 0.1f, 0.5f);

	// Use this for initialization
	void Start ()
    {
        player = this.GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //TODO: Raycast check interactability.
        RaycastHit hit;
        Debug.DrawRay(targetCamera.ViewportToWorldPoint(viewpoint), targetCamera.transform.TransformDirection(Vector3.forward - (Vector3.down * 0.2f)) * 2, Color.red, 1);

        if (Physics.Raycast(targetCamera.ViewportToWorldPoint(viewpoint), targetCamera.transform.TransformDirection(Vector3.forward - (Vector3.down * 0.2f)), out hit, 2))
        {
            if (hit.transform.GetComponent<Interactable>() != null)
            {
                hit.transform.GetComponent<Interactable>().LookAt();
                if (Input.GetKeyDown(KeyCode.E))
                {
                    if (hit.transform.GetComponent<HoldingItem>() != null)
                    {
                        hit.transform.GetComponent<HoldingItem>().PickupItem(player);
                    }
                    else
                    {
                        hit.transform.GetComponent<Interactable>().Interact(player);
                    }
                    Debug.Log(hit.transform.name);
                }
            }
            else
            {
                InteractText.instance.Text.text = "";
            }
        }
        else
        {
            InteractText.instance.Text.text = "";
        }
    }
}
