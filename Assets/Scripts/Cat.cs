﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class Cat : Interactable
{
    [Header("Main Values")]
    [SerializeField]
    private float hunger;
    [SerializeField]
    private float thirst;
    [SerializeField]
    private float sickness;
    [SerializeField]
    private float peeNeeds;
    [SerializeField]
    private float pooNeeds;
    [SerializeField]
    private float lonelyness;
    [SerializeField]
    private Bowl targetBowl;
    [SerializeField]
    AudioClip[] meowSounds;

    [SerializeField]
    private Transform destination;
    NavMeshAgent navMeshAgent;

    public bool isdead;

    BowlType searchfor = BowlType.None;

    public float Hunger
    {
        get
        {
            return hunger;
        }

        set
        {
            if (value < 0)
            {
                hunger = 0;
            }

            else if (value > 1)
            {
                hunger = 1;
            }

            else
            {
                hunger = value;
            }
        }
    }

    public float Thirst
    {
        get
        {
            return thirst;
        }

        set
        {
            if (value < 0)
            {
                thirst = 0;
            }

            else if (value > 1)
            {
                thirst = 1;
            }

            else
            {
                thirst = value;
            }
        }

    }

    public float Sickness
    {
        get
        {
            return sickness;
        }

        set
        {
            if (value < 0)
            {
                sickness = 0;

                isdead = true;
            }

            else if (value > 1)
            {
                sickness = 1;
            }

            else
            {
                sickness = value;
            }
        }
    }

    public float PeeNeeds
    {
        get
        {
            return peeNeeds;
        }

        set
        {
            if (value < 0)
            {
                peeNeeds = 0;
            }

            else if (value > 1)
            {
                peeNeeds = 1;
            }

            else
            {
                peeNeeds = value;
            }
        }
    }

    public float PooNeeds
    {
        get
        {
            return pooNeeds;
        }

        set
        {
            if (value < 0)
            {
                pooNeeds = 0;
            }

            else if (value > 1)
            {
                pooNeeds = 1;
            }

            else
            {
                pooNeeds = value;
            }
        }
    }

    public float Lonelyness
    {
        get
        {
            return lonelyness;
        }

        set
        {
            if (value < 0)
            {
                lonelyness = 0;
            }
            else if (value > 1)
            {
                lonelyness = 1;
            }
            else
            {
                lonelyness = value;
            }
        }
    }

    void Start()
    {
        StartCoroutine(DecreaseNumbers());
        StartCoroutine(MakeMeowSounds());

        
        navMeshAgent = this.GetComponent<NavMeshAgent>();
        if (navMeshAgent == null)
        {
            Debug.LogError("Nevmash is not on cat nmbr" + gameObject.name);
        }
        else
        {
            //setDestination();
        }
    }

    void Update()
    {
        if (Hunger <= 0 && Thirst <= 0)
        {
            sickness -= 1 * Time.deltaTime;
        }

        if (sickness <= 0)
        {
            Time.timeScale = 0;

            GameOverUI.instance.gameObject.SetActive(true);
        }

        if (targetBowl != null && targetBowl.Fullness > 0)
        {
            if (targetBowl.bowlType == BowlType.Food)
            {
                if (Vector3.Distance(this.transform.position, targetBowl.transform.position) < 1)
                {
                    targetBowl.UseBowl(this);
                }
                if (hunger > 0.95f)
                {
                    targetBowl = null;
                }
            }
            else if (targetBowl.bowlType == BowlType.Water)
            {
                if (Vector3.Distance(this.transform.position, targetBowl.transform.position) < 1)
                {
                    targetBowl.UseBowl(this);
                }
                if (thirst > 0.95f)
                {
                    targetBowl = null;
                }
            }
            return;
        }
        if (Hunger < 0.5f)
        {
            navMeshAgent.SetDestination(CheckSurounding(BowlType.Food));
        }
        else if (Thirst < 0.5f)
        {
            navMeshAgent.SetDestination(CheckSurounding(BowlType.Water));
        }
        else
        {
            if (Vector3.Distance(this.transform.position, navMeshAgent.destination) <= 2 || navMeshAgent.velocity.magnitude <= 0.1f)
            {
                
                Vector3 randomDirection = Random.insideUnitSphere * 10;
                randomDirection += transform.position;
                NavMeshHit hit;
                Vector3 finalPosition = Vector3.zero;
                if (NavMesh.SamplePosition(randomDirection, out hit, 10, 1))
                {
                    finalPosition = hit.position;
                }
                navMeshAgent.SetDestination(finalPosition);
            }
        }
    }

    private Vector3 CheckSurounding(BowlType type)
    {
        Vector3 targetVector = new Vector3(100000, 100000, 100000);
        Bowl[] bowls = GameObject.FindObjectsOfType<Bowl>().Where(b => b.bowlType == type && b.Fullness > 0).ToArray();
        if (bowls != null)
        {
            for (int i = 0; i < bowls.Length; i++)
            {
                if (Vector3.Distance(bowls[i].transform.position, transform.position) < Vector3.Distance(targetVector, transform.position))
                {
                    targetVector = bowls[i].transform.position;
                    targetBowl = bowls[i];
                }
            }
        }

        return targetVector;
    }

    IEnumerator MakeMeowSounds()
    {
        while(!isdead)
        {
            if (Hunger <= 0.25 || Thirst <= 0.25)
            {
                this.GetComponent<AudioSource>().PlayOneShot(meowSounds[Random.Range(0, meowSounds.Length)]);
            }
            yield return new WaitForSeconds(5);
        }

        yield return null;
    }

    IEnumerator DecreaseNumbers()
    {
        while (true)
        {
            yield return new WaitForSeconds(.5f);
            Hunger -= Random.Range(0.001f, 0.01f);
            Thirst -= Random.Range(0.001f, 0.01f);
            Lonelyness -= Random.Range(0.001f, 0.01f);
            PeeNeeds -= Random.Range(0.001f, 0.01f);
            PooNeeds -= Random.Range(0.001f, 0.01f);
        }
    }
}
