﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sink : Interactable
{
    protected override void Start()
    {

    }

    protected override void Update()
    {

    }

    public override void Interact(Player player)
    {
        base.Interact(player);

        if (player.CurrentlyHeldItem is Bowl)
        {
            player.CanPickedup = false;
            (player.CurrentlyHeldItem as Bowl).FillBowl(BowlType.Water);
        }
    }

    public override void LookAt()
    {
        base.LookAt();
        InteractText.instance.Text.text = "A sink to fill the bowls with water.";
    }
}
