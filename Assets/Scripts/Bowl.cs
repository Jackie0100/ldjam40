﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bowl : HoldingItem
{
	[SerializeField]
	public BowlType bowlType;
    [SerializeField]
    private float fullness;
    [SerializeField]
    GameObject[] fullnessFoodStages;
    [SerializeField]
    GameObject[] fullnessWaterStages;


    public override Renderer TargetRenderer
    {
        get
        {
            targetRenderer = GetBowlStages[Mathf.CeilToInt(Fullness * (GetBowlStages.Length - 1))].transform.GetChild(0).GetComponent<Renderer>();
            return targetRenderer;
        }

        set
        {
            base.TargetRenderer = value;
        }
    }

    public GameObject[] GetBowlStages
    {
        get
        {
            if (bowlType == BowlType.Food)
            {
                return fullnessFoodStages;
            }
            else
            {
                return fullnessWaterStages;
            }
        }

        set
        {
            fullnessFoodStages = value;
        }
    }

    public float Fullness
    {
        get
        {
            return fullness;
        }

        set
        {
            if (value < 0)
            {
                fullness = 0;
            }
            else if (value > 1)
            {
                fullness = 1;
            }
            else
            {
                fullness = value;
            }
        }
    }

    // Use this for initialization
    protected override void Start ()
    {
		
	}

    // Update is called once per frame
    protected override void Update ()
    {
		
	}

    public override void Interact(Player player)
    {
        base.Interact(player);

        if (player.CurrentlyHeldItem is CatFood)
        {
            player.CanPickedup = false;
            this.FillBowl(BowlType.Food);
        }
    }

    public override void LookAt()
    {
        base.LookAt();
        InteractText.instance.Text.text = "A bowl";
    }

    public override void PickupItem(Player player)
    {
        if (player.CurrentlyHeldItem is CatFood)
        {
            Interact(player);
        }
        else
        {
            player.PickupItem(this);
        }
    }

   public void UseBowl(Cat targetCat)
	{
        GetBowlStages[Mathf.CeilToInt(Fullness * (GetBowlStages.Length - 1))].SetActive(false);

        //TODO: Cat eats food :D
        if (bowlType == BowlType.Water)
        {
            targetCat.Thirst += 0.1f * Time.deltaTime;
        }
        if (bowlType == BowlType.Food)
        {
            targetCat.Hunger += 0.1f * Time.deltaTime;
        }
        Fullness -= 0.025f * Time.deltaTime;

        GetBowlStages[Mathf.CeilToInt(Fullness * (GetBowlStages.Length - 1))].SetActive(true);
    }

    public void FillBowl(BowlType bowlType)
    {
        if (this.bowlType != bowlType)
        {
            GetBowlStages[Mathf.CeilToInt(Fullness * (GetBowlStages.Length - 1))].SetActive(false);
            this.bowlType = bowlType;
        }

        Fullness = 1;
        GetBowlStages[GetBowlStages.Length - 1].SetActive(true);
    }
}

public enum BowlType
{
	None, Water, Food,
}