﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    HoldingItem currentlyHeldItem;
    [SerializeField]
    Transform hand;

    bool canPickedup = false;

    public HoldingItem CurrentlyHeldItem
    {
        get
        {
            return currentlyHeldItem;
        }
        private set
        {
            currentlyHeldItem = value;
        }
    }

    public bool CanPickedup
    {
        get
        {
            return canPickedup;
        }
        set
        {
            canPickedup = value;
        }
    }

    // Use this for initialization
    void Start ()
	{
        Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.E) && CanPickedup)
        {
            if (CurrentlyHeldItem != null)
            {
                PutDownItem(CurrentlyHeldItem);
            }
        }

        if (!CanPickedup)
        {
            CanPickedup = true;
        }
	}

    internal void PickupItem(HoldingItem holdingItem)
    {
        if (CurrentlyHeldItem != null)
        {
            PutDownItem(CurrentlyHeldItem);
        }
        CurrentlyHeldItem = holdingItem;
        holdingItem.transform.SetParent(hand, false);
        holdingItem.transform.localPosition = Vector3.zero;

        CanPickedup = false;
    }

    internal void PutDownItem(HoldingItem holdingItem)
    {
        Vector3 dropPos = this.transform.position;
        CurrentlyHeldItem.transform.SetParent(null);
        dropPos += this.transform.TransformDirection(Vector3.forward);
        dropPos = new Vector3(dropPos.x, 0, dropPos.z);
        if (CurrentlyHeldItem.GetComponent<Renderer>() != null)
        {
            dropPos += new Vector3(0, CurrentlyHeldItem.GetComponent<Renderer>().bounds.size.y * 0.5f, 0);
        }
        else if (CurrentlyHeldItem.TargetRenderer != null)
        {
            dropPos += new Vector3(0, CurrentlyHeldItem.TargetRenderer.bounds.size.y * 0.5f, 0);
        }

        CurrentlyHeldItem.transform.position = dropPos;
        CurrentlyHeldItem = null;
    }
}
