﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorUI : MonoBehaviour
{
    [SerializeField]
    Text randomtalk;
    [SerializeField]
    [Multiline]
    string[] randomQuotes;

    private void OnEnable()
    {
        randomtalk.text = randomQuotes[Random.Range(0, randomQuotes.Length)];
        Cursor.lockState = CursorLockMode.None;
    }

    public void CloseMenu()
    {
        Cursor.lockState = CursorLockMode.Locked;
        this.gameObject.SetActive(false);
    }
}
