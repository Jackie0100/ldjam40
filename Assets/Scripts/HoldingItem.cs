﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldingItem : Interactable
{
    [SerializeField]
    protected Renderer targetRenderer;

    public virtual Renderer TargetRenderer
    {
        get
        {
            return targetRenderer;
        }
        set
        {
            targetRenderer = value;
        }
    }

    // Use this for initialization
    protected override void Start ()
	{
		
	}

    // Update is called once per frame
    protected override void Update ()
	{
		
	}

    public virtual void PickupItem(Player player)
    {
        player.PickupItem(this);
    }

    public virtual void PutdownItem(Player player)
    {
        player.PutDownItem(this);
    }

    public override void Interact(Player player)
    {
        base.Interact(player);
    }

    public override void LookAt()
    {
        base.LookAt();
    }
}
