﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class InteractText : MonoBehaviour
{
    public static InteractText instance;

    public Text Text { get { return this.GetComponent<Text>(); } }

	// Use this for initialization
	void Start ()
    {
        instance = this;
        Text.text = "";
	}
}
