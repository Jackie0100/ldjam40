﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenClose : MonoBehaviour {
    public Animator anim;
    public bool isOpen = false;
    public float RotationAmount;
	
    void Start()
    {
        anim = this.GetComponent<Animator>();
        anim.SetBool("Open", false);
        anim.SetBool("Close", true);
    }

	void Update ()
    {
        if (Input.GetKeyUp(KeyCode.A))
        {
            CloseOpenDoor();
        }
    }

    void CloseOpenDoor()
    {
        if (isOpen)
        {
            anim.SetBool("Open", false);
            anim.SetBool("Close", true);
            isOpen = false;
        }
        else
        {
            anim.SetBool("Open", true);
            anim.SetBool("Close", false);
            isOpen = true;
        }
    }
}
