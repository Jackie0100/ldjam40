﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CatMovement : MonoBehaviour
{
    [SerializeField]
    private Transform destination;
    NavMeshAgent navMeshAgent;

	void Start ()
    {
        navMeshAgent = this.GetComponent<NavMeshAgent>();

        if (navMeshAgent == null)
        {
            Debug.LogError("Nevmash is not on cat nmbr" + gameObject.name);
        }

        else
        {
            setDestination();
        }
    }
	
    void Update ()
        {
		
	    }
    private void setDestination()
    {
        if(destination != null)
        {
            Vector3 targetVector = destination.transform.position;
            navMeshAgent.SetDestination(targetVector);
        }
    }

    


}
