﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    GameObject mainMenu;
    [SerializeField]
    GameObject settings;
    [SerializeField]
    GameObject Credits;

    public void NewGame()
    {
        SceneManager.LoadScene("MainLevel");
    }

    public void Showsettings()
    {
        mainMenu.SetActive(false);
        settings.SetActive(true);
        Credits.SetActive(false);
    }

    public void ShowCredits()
    {
        mainMenu.SetActive(false);
        settings.SetActive(false);
        Credits.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ShowMainMenu()
    {
        mainMenu.SetActive(true);
        settings.SetActive(false);
        Credits.SetActive(false);
    }
}
