﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maindoor : Interactable
{
    [SerializeField]
    GameObject cat;
    [SerializeField]
    DoorUI doorUI;
    bool doorbellRinging = false;

    // Use this for initialization
    protected override void Start()
    {
        StartCoroutine(RingDoorBell());
    }

    // Update is called once per frame
    protected override void Update()
    {

    }

    public override void Interact(Player player)
    {
        base.Interact(player);

        if (doorbellRinging)
        {
            doorUI.gameObject.SetActive(true);
            player.CanPickedup = false;
            doorbellRinging = false;
            Instantiate(cat, new Vector3(1, 0.002f, -7), cat.transform.rotation);
        }
    }

    public override void LookAt()
    {
        base.LookAt();
        InteractText.instance.Text.text = "My main door, I should answer the door bell.";
    }

    IEnumerator RingDoorBell()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(30, 60));
            doorbellRinging = true;
            while (doorbellRinging)
            {
                this.GetComponent<AudioSource>().Play();

                yield return new WaitForSeconds(2);
            }
        }
        yield return null;
    }
}
