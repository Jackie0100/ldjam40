﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatFood : HoldingItem
{
    protected override void Start()
    {

    }

    protected override void Update()
    {

    }

    public override void Interact(Player player)
    {
        base.Interact(player);

        if (player.CurrentlyHeldItem is Bowl)
        {
            player.CanPickedup = false;
            (player.CurrentlyHeldItem as Bowl).FillBowl(BowlType.Food);
        }
    }

    public override void PickupItem(Player player)
    {
        if (player.CurrentlyHeldItem is Bowl)
        {
            Interact(player);
        }
        else
        {
            player.PickupItem(this);
        }
    }

    public override void PutdownItem(Player player)
    {
        player.PutDownItem(this);
    }

    public override void LookAt()
    {
        base.LookAt();
        InteractText.instance.Text.text = "Premium Catfood for my lovelies!";
    }
}
